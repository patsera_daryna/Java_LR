package org.hneu.lr1;

import java.util.Scanner;

/**
 * Created by Дарина on 06.04.2018.
 */
public class Calculator {

    private static final String DIGIT_REGEX = "(^[0-4]\\d*)";

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String operation;
        int firstArgument;
        int secondArgument;

        do {
            System.out.println("Choose operations, please:" +
                    "\n1: sum," +
                    "\n2: subtraction," +
                    "\n3: multiplication," +
                    "\n4: division," +
                    "\n0: exit");
            operation = scanner.nextLine();
        } while (!operation.matches(DIGIT_REGEX));

        System.out.println("Enter first argument, please: ");
        firstArgument = scanner.nextInt();
        System.out.println("Enter second argument, please: ");
        secondArgument = scanner.nextInt();

        scanner.close();
    }

}
